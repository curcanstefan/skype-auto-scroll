// ==UserScript==
// @name         Skype Auto Scroll
// @namespace    https://bitbucket.org/curcanstefan/
// @version      0.1
// @description  auto scroll skype web version when images are loaded; auto scroll runs as long as you don't scroll up
// @author       Stefan Curcan
// @match        https://web.skype.com*
// @grant        none
// ==/UserScript==
//@require http://code.jquery.com/jquery-latest.js


// load jQuery and execute the main function
addJQuery(waitForInit);

function addJQuery(callback) {
    var script = document.createElement("script");
    script.setAttribute("src", "https://code.jquery.com/jquery-latest.js");
    script.addEventListener('load', function() {
        var script = document.createElement("script");
        script.textContent = "window.jQ=jQuery.noConflict(true);(" + callback.toString() + ")();";
        document.body.appendChild(script);
    }, false);
    document.body.appendChild(script);
}

function waitForInit() {
    if (jQ('#shellSplashScreen').length) {
        setTimeout(waitForInit, 1000);
        console.log('not yet initiated');
        return null;
    }
    console.log('skype finally loaded');
    main();
}

window.main = function(){
  //Jquery Here
    // bind click on chat selector to reset global var
    var availableChats = jQ('.history.scrollable');
    chatScrolled = false;
    availableChats.on('click', function() {
        globalChatHeight = 0;
        chatScrolled = false;
        //console.log('chat changed');
    });
    
    // start trigger
  jQ(function(){
      setInterval(function() {
          //console.debug('checking for different height');
          //console.debug('scrollTop: ', jQ('.conversation.scrollable').scrollTop());
          
          // get chat height
          var conversation = jQ('.conversation.scrollable:visible');
          var chat = conversation.find('div.messageHistory:visible');
          
          var currentHeight = parseInt(chat.css('height')) || 0;
          var scrolledHeight = conversation.scrollTop();
          
          // set global var if not set
          if (typeof globalChatHeight == 'undefined' || !globalChatHeight) {
              //console.debug('setting global var');
              //console.debug('currentHeight: ', currentHeight);
              globalChatHeight = 1;
          }
          
          if (!chatScrolled && !scrolledHeight) {
              conversation.scrollTop(currentHeight);
          } else {
              chatScrolled = true;
          }
          
          // if global var < current height then scroll to bottom
          if (scrolledHeight &&
              globalChatHeight < currentHeight &&
              globalChatHeight - scrolledHeight < conversation.height())
          {
              //console.debug('currentScrollTop: ', scrolledHeight);
              //console.debug('globalChatHeight: ', globalChatHeight);
              //console.debug('currentHeight: ', currentHeight);
              //console.debug('scrolling to bottom');
              // scroll to bottom
              conversation.scrollTop(scrolledHeight + currentHeight - globalChatHeight);
              // and update global var
              //console.debug('scrolled to: ' , scrolledHeight + currentHeight - globalChatHeight );
              //console.debug('newScrollTop: ', conversation.scrollTop());
              globalChatHeight = currentHeight;
          }
      }, 100);
      console.log('sequence initiated');
  });

}